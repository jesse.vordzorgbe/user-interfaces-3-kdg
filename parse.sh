cat ./data/temp.json | jq '. | .items[] |
{
        id: 0,
        name: .name,
        description: [.artists[].name] | join(", "),
        price: (.duration_ms/1000*.track_number/10),
        inPromotion: .is_local,
        quantity: .popularity,
        image: .album.images[1].url,
        location: 0,
        properties: [{release_date: .album.release_date},
                     {genre: "other"}]
}'