# User Interfaces 3 - KdG

A store page frontend with simulated backend.
This was a project as part of a user interfaces class I took.

## Usage

1. `npm install` to install the dependencies
2. `npm run api` to start the simulated backend
3. `npm run dev` to start the dev server

The default port for dev server is set to 8080, this can be changed in `vite.config.ts`

The default port for the backend is 3000, this can be changed in the `package.json` file by adding `--port <port>`. More info on json-server [here](https://www.npmjs.com/package/json-server)