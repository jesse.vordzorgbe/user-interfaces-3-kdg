import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useState as useHookState } from '@hookstate/core'

import { AppShell, Burger, Button, Group, Header, MediaQuery, Navbar, Switch, NavLink, Stack, createStyles, Center, useMantineColorScheme } from "@mantine/core"
import { IconAdjustmentsHorizontal, IconLogout, IconMap, IconMoon, IconSun } from '@tabler/icons'

import AccountMenu from "../components/AccountMenu"
import settingsStore from '../contexts/SettingsStorage'

function NavLayout({ children }: any) {
    const { isManager, managerAccount } = useHookState(settingsStore)
    const [burgerOpened, setBurgerOpened] = useState(false)

    return (
        <AppShell navbarOffsetBreakpoint='sm'
            header={<NavbarHeader />}
            navbar={<NavbarModal />}
        >
            {children}
        </AppShell>
    )

    function NavbarModal() {
        return (
            <>
                <MediaQuery largerThan='sm' styles={{ display: 'none' }}>
                    <Navbar width={{ base: '100%', sm: 0 }} hidden={!burgerOpened}>
                        <NavLink component={Link} to='/map' label='Map view' icon={<IconMap size={15} />} />
                        <NavLink label='Settings' icon={<IconAdjustmentsHorizontal size={15} />}>
                            <Stack p='xs' justify='space-around'>
                                <Switch
                                    label='Manager permissions'
                                    checked={isManager.get()}
                                    onChange={() => isManager.set(!isManager.get())} />
                            </Stack>
                        </NavLink>
                        <NavLink component={Link} to='/' label={isManager.get() ? 'Log out' : 'Log in'} icon={<IconLogout size={15} />} />
                    </Navbar>
                </MediaQuery>
            </>
        )
    }

    function NavbarHeader() {
        const { colorScheme, toggleColorScheme } = useMantineColorScheme()

        return (
            <Header fixed p={10} height={50} >
                <Group position='apart'>
                    <Button component={Link} to='/shop' compact>Free Record Shop</Button>
                    <Group position='apart'>
                        <Switch
                            checked={colorScheme === 'dark'}
                            onChange={() => toggleColorScheme()}
                            onLabel={<IconMoon size={15} />}
                            offLabel={<IconSun size={15} />} />
                        <MediaQuery smallerThan="sm" styles={{ display: 'none' }}>
                            <Group>
                                <Button component={Link} to='/map' compact variant='light'>Map view</Button>
                                <AccountMenu />
                            </Group>
                        </MediaQuery>

                        <MediaQuery largerThan="sm" styles={{ display: 'none' }}>
                            <Burger opened={burgerOpened} onClick={() => setBurgerOpened(!burgerOpened)} size="sm" />
                        </MediaQuery>
                    </Group>
                </Group>
            </Header>
        )
    }
}

export default NavLayout