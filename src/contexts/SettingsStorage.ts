import { hookstate } from "@hookstate/core";
import { localstored } from "@hookstate/localstored";

const settingsStore = hookstate({
    managerAccount: 'manager@ui3store.be',
    isLoggedIn: false,
    isManager: false,
    darkMode: false
}, localstored({
    key: 'settings'
}))

export default settingsStore