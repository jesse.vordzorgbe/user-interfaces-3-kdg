import { Link, useParams } from "react-router-dom"

import { Alert, Button, Container, Group } from "@mantine/core"
import { IconArrowNarrowRight } from "@tabler/icons"

import ItemDetails from "../components/ItemDetails"
import NavLayout from "../layouts/NavLayout"

function Details() {
    const { id } = useParams()

    return (
        <NavLayout>
            <Container size='xl'>
                <ItemDetails id={parseInt(id!)} />
                <Group position='right'>
                    <Button component={Link} to={`/details/${id}/map`} rightIcon={<IconArrowNarrowRight />}>Map view</Button>
                </Group>
            </Container>
        </NavLayout>
    )
}

export default Details