import { useParams } from "react-router-dom"
import { useState } from "@hookstate/core"

import NavLayout from "../layouts/NavLayout"
import Blueprint from "../components/Blueprint"
import settingsStore from "../contexts/SettingsStorage"

function MapDetails() {
    const { id } = useParams()
    const { isManager } = useState(settingsStore)

    return (
        <NavLayout>
            <Blueprint managerView={isManager.get()} id={parseInt(id!)} />
        </NavLayout>
    )
}

export default MapDetails