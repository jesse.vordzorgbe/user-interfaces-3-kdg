import { useState } from "@hookstate/core";

import NavLayout from "../layouts/NavLayout";
import Blueprint from "../components/Blueprint";
import settingsStore from "../contexts/SettingsStorage";


function Map() {
    const { isManager } = useState(settingsStore)

    return (
        <NavLayout>
            {isManager.get()
                ? <Blueprint managerView={true} />
                : <Blueprint managerView={false} />}
        </NavLayout>
    )
}

export default Map