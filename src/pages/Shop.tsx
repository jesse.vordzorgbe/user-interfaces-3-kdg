import NavLayout from '../layouts/NavLayout';
import ShopBody from '../components/ShopBody';

/**
 * TODO: fetch data from here instead of ShopBody
 */

function Shop() {
    return (
        <NavLayout>
            <ShopBody />
        </NavLayout>
    )
}

export default Shop