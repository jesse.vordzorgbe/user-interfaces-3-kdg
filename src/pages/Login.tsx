import { useEffect } from 'react'
import axios from 'axios'
import useSWR from 'swr'

import { Box, Group, Image, Overlay, useMantineTheme } from '@mantine/core'
import { useViewportSize } from '@mantine/hooks'

import LoginForm from '../components/LoginForm'
import ShopItemType from '../models/ShopItem'
import './Login.css'

function Login() {
    const { data } = useSWR<ShopItemType[]>('/tracks', fetchData)

    const { width: w } = useViewportSize()
    const width = w < 800 ? w / 3 : w / 6

    async function fetchData(url: string) {
        const response = await axios.get(url)
        return await response.data
    }

    useEffect(() => {
        document.body.classList.add('overflow-hidden')
        return () => {
            document.body.classList.remove('overflow-hidden')
        }
    }, [])

    if (!data) return null

    return (
        <>
            <Box sx={{ width: '100%', height: '100vh', position: 'relative', zIndex: 5 }}>
                <LoginForm />
            </Box>
            <Group position='apart' spacing={0} sx={{ position: 'absolute', top: 0, left: 0, zIndex: -1, maxHeight: '100vh' }}>
                {data.map((record) => (
                    <Image p={0} width={width} src={record.image} />
                ))}
            </Group>
            <Overlay opacity={0.6} blur={10} color='#000000' zIndex={4} />
        </>
    )
}

export default Login