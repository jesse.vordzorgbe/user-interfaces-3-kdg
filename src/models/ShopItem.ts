type ShopItemType = {
    id: number;
    name: string;
    description: string;
    price: number;
    inPromotion: boolean;
    quantity: number;
    image: string;
    location: number[];
    properties: { [x: string]: string }[];
}

export default ShopItemType