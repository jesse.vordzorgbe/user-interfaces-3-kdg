import { useEffect, useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { useHookstate } from '@hookstate/core'

import { ColorScheme, ColorSchemeProvider, MantineProvider } from '@mantine/core'

import store from './contexts/SettingsStorage'
import Login from './pages/Login'
import Shop from "./pages/Shop";
import Map from "./pages/Map";

import './App.css'
import Details from "./pages/Details";
import MapDetails from "./pages/MapDetails";
import axios from "axios";
import { useLocalStorage } from "@mantine/hooks";

function App() {
  const [colorScheme, setColorScheme] = useLocalStorage<ColorScheme>({
    key: 'mantine-color-scheme',
    defaultValue: 'dark',
    getInitialValueInEffect: true,
  });

  function toggleColorScheme() {
    setColorScheme(colorScheme === 'dark' ? 'light' : 'dark')
    console.log('changed color');

  }

  axios.defaults.baseURL = 'http://localhost:3000'

  return (
    <BrowserRouter>
      <ColorSchemeProvider colorScheme={colorScheme} toggleColorScheme={toggleColorScheme}>
        <MantineProvider withNormalizeCSS withGlobalStyles theme={{ colorScheme }}>
          <Routes>
            <Route path='/' element={<Login />} />
            <Route path='/shop' element={<Shop />} />
            <Route path='/details/:id' element={<Details />} />
            <Route path='/details/:id/map' element={<MapDetails />} />
            <Route path='/map' element={<Map />} />
            <Route path='/map/:genre' element={<Map />} />
          </Routes>
        </MantineProvider>
      </ColorSchemeProvider>
    </BrowserRouter>
  )
}

export default App
