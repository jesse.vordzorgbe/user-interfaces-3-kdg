import { Center, Grid, Group, Image, Skeleton, Stack } from "@mantine/core"

function ItemDetailsPlaceholder() {
    return (
        <Grid gutter='xl' my='xs'>
            <Grid.Col sm={12} md={4}>
                <Center>
                    <Image src={null} width={300} height={300} withPlaceholder />
                </Center>
            </Grid.Col>
            <Grid.Col sm={12} md={8}>
                <Stack spacing={12}>
                    <Stack spacing={0}>
                        <Group mb={0} pb={0} position='apart'>
                            <Stack spacing={0}>
                                <Skeleton mt={30} height={12} width={250} />
                                <Skeleton mt={10} height={8} width={100} />
                            </Stack>
                            <Skeleton mt={10} height={12} width={75} />
                        </Group>
                    </Stack>
                    <Skeleton mt={30} height={8} width={'100%'} />
                    <Skeleton mt={0} height={8} width={'100%'} />
                    <Skeleton mt={0} height={8} width={'100%'} />
                    <Skeleton mt={0} height={8} width={'75%'} />
                </Stack>
            </Grid.Col>
        </Grid>
    )
}
export default ItemDetailsPlaceholder