import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useState } from "@hookstate/core";

import { Box, Button, Center, Container, Stack, TextInput, Divider, Space } from "@mantine/core"
import { useForm } from "@mantine/form"

import settingsStore from "../contexts/SettingsStorage";

function LoginForm() {
    const navigate = useNavigate()
    const { isLoggedIn, isManager } = useState(settingsStore)
    const form = useForm({
        initialValues: {
            email: ''
        },
        validate: {
            email: (value) => ('manager@ui3store.be' === value ? null : 'Invalid email')
        }
    })

    useEffect(() => {
        isLoggedIn.set(false)
        isManager.set(false)
    }, [])

    function handleSubmit(manager: boolean) {
        isLoggedIn.set(true)
        if (manager)
            isManager.set(true)
        navigate('/shop')
    }

    return (
        <Container size='xs' p='md' style={{ height: '100vh', display: 'flex', alignItems: 'center' }}>
            <Box sx={(theme) => ({ backgroundColor: theme.colorScheme === 'dark' ? '#1A1B1E' : 'white', borderRadius: 10, padding: 20, width: '100%' })}>
                <Center>
                    <h2 style={{ marginTop: '0.4rem' }}>Login</h2>
                </Center>
                <form onSubmit={form.onSubmit(() => handleSubmit(true))}>
                    <Stack>
                        <TextInput label='Enter your email' withAsterisk placeholder='Email' {...form.getInputProps('email')} />
                        <TextInput type='password' label='Enter your password' placeholder='Password' />
                        <Space />
                        <Button variant='gradient' fullWidth type='submit'>Login</Button>
                        <Divider label='OR' labelPosition='center' labelProps={{ color: 'dimmed' }} />
                        <Button variant='light' fullWidth onClick={() => handleSubmit(false)}>Continue as guest</Button>
                    </Stack>
                </form>
            </Box>
        </Container>
    )
}

export default LoginForm