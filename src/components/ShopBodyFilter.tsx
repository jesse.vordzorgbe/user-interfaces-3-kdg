import { useState } from "react"

import { Grid, MultiSelect, Stack, Switch, TextInput } from "@mantine/core"
import { useDebouncedValue } from "@mantine/hooks"

type ShopBodyFilterProps = {
    setLink: (arg: string) => void
}

function ShopBodyFilter({ setLink }: ShopBodyFilterProps) {
    const [value, setValue] = useState('')
    const selectData = [
        { value: 'react', label: 'React' },
        { value: 'ng', label: 'Angular' },
        { value: 'svelte', label: 'Svelte' },
        { value: 'vue', label: 'Vue' },
        { value: 'riot', label: 'Riot' },
        { value: 'next', label: 'Next.js' },
        { value: 'blitz', label: 'Blitz.js' },
    ]

    return (
        <>
            <Grid pt='sm'>
                <Grid.Col sm={12} md={8}>
                    <TextInput placeholder='Search' />
                </Grid.Col>
                <Grid.Col sm={12} md={4}>
                    <MultiSelect searchable placeholder='Category' data={selectData} />
                </Grid.Col>
            </Grid>
        </>
    )
}

export default ShopBodyFilter