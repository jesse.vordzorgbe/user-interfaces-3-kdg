import { useEffect, useState } from "react"
import { Link, useParams } from "react-router-dom"
import useSWR from "swr"
import axios from "axios"

import { Alert, Box, Button, Center, Container, Group } from "@mantine/core"
import { IconArrowNarrowLeft } from "@tabler/icons"
import { useDebouncedState, useElementSize } from "@mantine/hooks"

import ShopItemType from "../models/ShopItem"
import BlueprintItem from "./BlueprintItem"
import ItemDetails from "./ItemDetails"
import ItemEditModal from "./ItemEditModal"
import useFetchData from "../hooks/useFetchData"

// type BlueprintProperties = {
//     rowAmount: number,
//     columnAmount: number,
// }

type BlueprintProps = {
    id?: number,
    managerView: boolean
}

function Blueprint({ id, managerView }: BlueprintProps) {
    const { data: d, error, } = useSWR<ShopItemType[]>('/tracks', useFetchData)
    const { genre } = useParams()

    let data = d

    const { ref, width, height } = useElementSize()
    const [debouncedWidth, setDebouncedWidth] = useDebouncedState(1, 200)
    const [debouncedHeight, setDebouncedHeight] = useDebouncedState(1, 200)

    const [activeCard, setActiveCard] = useState<number | undefined>(id)
    const [modalOpened, setModalOpened] = useState(false)

    useEffect(() => {
        if (debouncedWidth !== width) {
            setDebouncedWidth(width)
            setDebouncedHeight(height)
        }
    }, [width, height])

    if (error) return <div>error...</div>

    if (!data) {
        return (
            <Container size='xs' my='md'>
                <Alert title='Loading'>Items are loading...</Alert>
            </Container>
        )
    }

    if (genre !== undefined) {
        data = data.filter(i => i.properties[1].genre === genre)
    }

    return (
        <Container p={0} size={1200}>
            <Box ref={ref} sx={(theme) => ({ backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.dark[2], height: debouncedWidth / (5 / 3) + debouncedWidth / 10 * 2, position: 'relative', zIndex: 1 })}>
                {data?.map((record) => (
                    <BlueprintItem key={record.id} record={record} managerView={managerView} activeCard={activeCard} setActiveCard={setActiveCard} width={debouncedWidth} height={debouncedHeight} />
                ))}
            </Box>
            <Group py='xs' position='apart'>
                <Button disabled={activeCard === undefined} leftIcon={<IconArrowNarrowLeft />} component={Link} to={`/details/${activeCard}`}>Details</Button>
                <Group>
                    <Button disabled={activeCard === undefined || managerView !== true} onClick={() => setModalOpened(true)}> Edit</Button>
                    <Button disabled={activeCard === undefined} onClick={() => setActiveCard(undefined)}>Deselect</Button>
                </Group>
            </Group>
            {activeCard !== undefined && <ItemDetails id={activeCard} />}
            {
                activeCard !== undefined &&
                <ItemEditModal opened={modalOpened} setOpened={setModalOpened} record={data.find(i => i.id === activeCard)!} />
            }
        </Container >
    )
}

export default Blueprint