import { useState } from 'react'
import { Link } from 'react-router-dom'
import { useState as useHookState } from '@hookstate/core'

import { BackgroundImage, Badge, Box, Card, Group, Image, Overlay, Paper, Stack, Text, Transition, useMantineColorScheme } from '@mantine/core'

import settingsStore from '../contexts/SettingsStorage'
import ShopItemType from '../models/ShopItem'

type ShopItemPreviewProps = {
    shopItem: ShopItemType
}

function ShopItemPreview({ shopItem }: ShopItemPreviewProps) {
    const { colorScheme, toggleColorScheme } = useMantineColorScheme()
    const [cardSize, setCardSize] = useState(300)

    return (
        <Card onMouseEnter={() => setCardSize(300)} onMouseLeave={() => setCardSize(300)} component={Link} to={`/details/${shopItem.id}`} style={{ width: cardSize, height: '100%' }} shadow='sm' radius='sm' p={0}>
            <Card.Section sx={{ position: 'relative', zIndex: 5 }}>
                {shopItem.inPromotion &&
                    <Badge style={{ boxShadow: '0 0 10px -5px #000000' }} variant="gradient" gradient={{ from: 'red', to: 'pink', deg: 105 }} sx={{ position: 'absolute', margin: 10, paddingBottom: 2, zIndex: 5 }}>promo</Badge>}
                <Image height={cardSize} src={shopItem.image} alt={shopItem.name} />
            </Card.Section>
            <BackgroundImage src={shopItem.image}>
                <Card.Section p={8}>
                    <Box sx={{ width: '100%', height: '100%', zIndex: 5, position: 'relative' }}>
                        <Stack px={5} spacing={0} justify='space-around'>
                            <Group position='apart' noWrap>
                                <Text lineClamp={1} color={colorScheme === 'dark' ? '' : 'dark'} size='lg' weight={700}>{shopItem.name}</Text>
                                <Text>€{shopItem.price.toFixed(2)}</Text>
                            </Group>
                            <Group mx={1}>
                                <Text color={colorScheme === 'dark' ? '' : 'dark'} lineClamp={1}>{shopItem.description}</Text>
                            </Group>
                        </Stack>
                    </Box>
                </Card.Section>
                <Overlay opacity={0.5} blur={20} color={colorScheme === 'dark' ? '#000000' : '#eeeeee'} zIndex={1} />
            </BackgroundImage>
        </Card>
    )
}

export default ShopItemPreview