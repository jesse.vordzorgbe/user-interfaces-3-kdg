import useSWR from "swr"
import axios from "axios"

import { Container, Grid, Group, Image, Stack, Text } from "@mantine/core"

import ItemDetailsPlaceholder from "./ItemDetailsPlaceholder"
import ShopItemType from "../models/ShopItem"
import useFetchData from "../hooks/useFetchData"

type ItemDetailsProps = {
    id: number
}

function ItemDetails({ id }: ItemDetailsProps) {
    const { data: record, error, } = useSWR<ShopItemType>(`/tracks/${id}`, useFetchData)

    if (!record) {
        return (
            <Container size='xl' my='md'>
                <ItemDetailsPlaceholder />
            </Container>
        )
    }
    return (
        <Grid gutter='xl' my='xs'>
            <Grid.Col sm={12} md={4}>
                <Group position='center'>
                    <Image src={record.image} width={300} />
                </Group>
            </Grid.Col>
            <Grid.Col sm={12} md={8}>
                <Stack>
                    <Stack spacing={0}>
                        <Group mb={0} pb={0} position='apart'>
                            <Stack justify='flex-start' spacing={0} pt={0}>
                                <h2 style={{ marginBottom: 0 }}>{record.name}</h2>
                                <Text color='dimmed'>{record.description}</Text>
                            </Stack>
                            <h2>€ {record.price.toFixed(2)}</h2>
                        </Group>
                    </Stack>
                    <Text>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis temporibus laborum, eligendi itaque facilis dolore, beatae similique esse nesciunt, iste omnis labore debitis assumenda mollitia reiciendis aspernatur ipsa possimus voluptatum neque aut soluta. Dignissimos esse ut fugit, culpa, veniam eum qui accusamus, dolorem quo blanditiis deleniti quibusdam facilis eligendi sint.</Text>
                    <Stack>
                        {/* 
                        TODO: ask about backend to frontend
                         */}
                        <Text color='dimmed'>Released: {record.properties[0].release_date}</Text>
                        <Text color='dimmed'> Genre: {record.properties[1].genre}</Text>
                    </Stack>
                </Stack>
            </Grid.Col>
        </Grid>
    )
}

export default ItemDetails