import { Avatar, Card, Group, Overlay, useMantineTheme } from "@mantine/core"
import ShopItemType from "../models/ShopItem"

type BlueprintItemProps = {
    managerView: boolean,
    record: ShopItemType,
    activeCard?: number,
    setActiveCard: (arg0: number) => void
    width: number,
    height: number
}

function BlueprintItem({ record, activeCard, setActiveCard, managerView, width }: BlueprintItemProps) {
    const theme = useMantineTheme()
    const x = record.location[1]
    const y = record.location[0]
    const a = width / 10
    const hallwaySpacing = 1.5
    let locationTop = y * a + y * (hallwaySpacing * a) + a
    const locationLeft = x * a

    let backgroundColor = '';

    if (managerView && activeCard === undefined) {
        backgroundColor = record.quantity < 10 ? theme.colors.red[8] : record.quantity < 15 ? theme.colors.orange[7] : '#2f9e44'
    }

    if (record.id === activeCard) {
        backgroundColor = theme.colors.blue[5]
    }

    if (record.location[3] === 1) locationTop = locationTop + a / 2


    return (
        <>
            <Card onClick={handleClick} p={5} radius={0} withBorder sx={{ width: a, height: a / 2, position: 'absolute', top: locationTop, left: locationLeft, overflow: 'hidden' }}>
                <Group sx={{ zIndex: 3, position: 'relative' }}>
                    <Avatar component='a' size={a / (80 / 28)} radius='xs' src={record.image} />
                    {record.quantity.toString()}
                </Group>
                <Overlay color={backgroundColor} opacity={theme.colorScheme === 'dark' ? 0.35 : 0.6} zIndex={0} />
            </Card>
        </>
    )

    function handleClick() {
        setActiveCard(record.id)
    }
}

export default BlueprintItem