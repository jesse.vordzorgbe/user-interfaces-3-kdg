import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { useState as useHookState } from '@hookstate/core'

import { Button, Menu, Modal, Switch } from '@mantine/core'

import settingsStore from '../contexts/SettingsStorage'

function AccountMenu() {
    const [opened, setOpened] = useState(false)
    const { isManager, managerAccount } = useHookState(settingsStore)
    const [checked, setChecked] = useState(isManager.get())

    function handleChange() {
        setChecked(e => !e)
    }

    useEffect(() => {
        setTimeout(() => {
            isManager.set(checked)
        }, 120)
    }, [checked])

    return (
        <>
            <Menu shadow='sm' width={150} position='bottom-end'>
                <Menu.Target>
                    <Button compact variant='light'>{isManager.get() ? managerAccount.get() : 'Account'}</Button>
                </Menu.Target>
                <Menu.Dropdown>
                    <Menu.Label>Account</Menu.Label>
                    <Menu.Item onClick={() => setOpened(true)}>Settings</Menu.Item>
                    {isManager.get()
                        ? <Menu.Item component={Link} to='/'>Log out</Menu.Item>
                        : <Menu.Item component={Link} to='/'>Log in</Menu.Item>}
                </Menu.Dropdown>
            </Menu>
            <Modal overlayBlur={5} opened={opened} onClose={() => setOpened(false)} centered title='Settings'>
                <Switch
                    label='Manager permissions'
                    checked={checked}
                    onChange={() => setChecked(e => !e)} />
            </Modal>
        </>
    )
}

export default AccountMenu