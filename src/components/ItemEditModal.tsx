import { useEffect, useState } from "react"
import { mutate } from "swr"
import axios from "axios"

import { Button, Container, Group, Modal, Stack, Switch, TextInput } from "@mantine/core"

import ShopItemType from "../models/ShopItem"

type ItemEditModalProps = {
    opened: boolean,
    setOpened: (arg: boolean) => void,
    record: ShopItemType
}

function ItemEditModal({ opened, setOpened, record: r }: ItemEditModalProps) {
    const [record, setRecord] = useState(r)
    const [location, setLocation] = useState(r.location.map(i => i.toString()))

    function handleSubmit() {
        mutate(`/tracks/${record.id}`, updateRecord())
        setOpened(false)
    }

    function handlePositionChange(index: number, e: React.ChangeEvent<HTMLInputElement>) {
        // const loc = location.map((l, i) => {
        //     if (i === index) {
        //         return e.target.value
        //     } else {
        //         return l
        //     }
        // });
        // setLocation(loc)
    }

    useEffect(() => {
        if (record.id !== r.id) {
            setRecord(r)
            setLocation(r.location.map(i => i.toString()))
        }
        setRecord({ ...r, location: location.map(i => parseFloat(i)) })
    }, [opened, location])

    return (
        <Modal opened={opened} onClose={() => setOpened(false)} withCloseButton={false} closeOnClickOutside={false}>
            <Container>
                <h2>
                    {record.name}
                </h2>
                <Stack pb='md'>
                    <TextInput value={record.name} onChange={(e) => setRecord({ ...record, name: e.target.value })} label='Title' required />
                    <TextInput value={record.description} onChange={(e) => setRecord({ ...record, description: e.target.value })} label='Description' />
                    <Group position='apart'>
                        <TextInput sx={{ width: '65%' }} value={record.price} onChange={(e) => setRecord({ ...record, price: parseInt(e.target.value) })} label='Price' />
                        <Switch pt={25} label='Promo' checked={record.inPromotion} onChange={(e) => setRecord({ ...record, inPromotion: e.currentTarget.checked })} />
                    </Group>
                    <TextInput value={record.image} onChange={(e) => setRecord({ ...record, image: e.target.value })} label='Image' />
                    <TextInput value={record.quantity} onChange={(e) => setRecord({ ...record, quantity: parseInt(e.target.value) })} label='Quantity' />
                    <TextInput value={record.properties[1].genre} disabled label='Departement' />
                    Location
                    <Group noWrap>
                        {location.map((item, index) => (
                            <TextInput key={index} value={item.toString()} onChange={(e) => handlePositionChange(index, e)} />
                        ))}
                    </Group>
                </Stack>
                <Group position='right'>
                    <Button onClick={() => setOpened(false)} variant='light'>Cancel</Button>
                    <Button variant='gradient' onClick={handleSubmit}>Save</Button>
                </Group>
            </Container>
        </Modal>
    )

    async function updateRecord() {
        const res = await axios.put(`/tracks/${record.id}`, record)
        return res.data
    }
}

export default ItemEditModal