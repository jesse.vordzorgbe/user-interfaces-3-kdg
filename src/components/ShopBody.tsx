import { useState } from "react"
import useSWR from "swr"
import axios from "axios"

import { Container, Alert, Group } from "@mantine/core"
import { Carousel } from "@mantine/carousel"

import ShopItemType from "../models/ShopItem"
import ShopItemPreview from "./ShopItemPreview"
import ShopBodyFilter from "./ShopBodyFilter"
import useFetchData from "../hooks/useFetchData"

function ShopBody() {
    const [link, setLink] = useState('/tracks')
    const { data, error } = useSWR<ShopItemType[]>(link, useFetchData)

    if (error) return <div>error...</div>

    if (!data) {
        return (
            <Container size='xs' my='md'>
                <Alert title='Loading'>Items are loading...</Alert>
            </Container>
        )
    }

    return (
        <Container size={1500}>
            <ShopBodyFilter setLink={setLink} />
            <h1>Items in promotion</h1>
            <Carousel slideSize={300} slideGap='md' loop >
                {data.filter(items => items.inPromotion).map((item: ShopItemType) => (
                    <Carousel.Slide key={item.id} pb='md'>
                        <ShopItemPreview key={item.id} shopItem={item} />
                    </Carousel.Slide>
                ))}
            </Carousel>
            <h1>Other Items</h1>
            <Group position='center'>
                {data.map((item: ShopItemType) => (
                    <ShopItemPreview key={item.id} shopItem={item} />
                ))}
            </Group>
        </Container >
    )
}

export default ShopBody