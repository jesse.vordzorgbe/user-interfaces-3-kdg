import { useEffect, useRef } from 'react';

function useAfterRender(func: () => void, deps: any[]) {
    const didMount = useRef(false);

    useEffect(() => {
        if (didMount.current) {
            func();
        } else {
            didMount.current = true;
        }
    }, deps);
}

export default useAfterRender;