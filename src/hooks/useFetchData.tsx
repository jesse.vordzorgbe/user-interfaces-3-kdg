import axios from 'axios'

async function useFetchData(url: string) {
    const response = await axios.get(url)
    return await response.data
}

export default useFetchData